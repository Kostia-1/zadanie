$(document).ready(function(){
  let $window = $(window),
  $document = $(document),
  $slider = $('#slides'),
  sliderMaxScrollLeft = $slider.prop('scrollWidth') - $window.width(),
  horizontal_scrolling_speed = 100,
  lastScrollTop = $(window).scrollTop(),
  targetOffset = $slider.offset().top - $window.height() / 2 + $slider.height() / 2;

$window.on({
  scroll: function() {
    let scrollTop = $window.scrollTop(),
      sliderScrollLeft = $slider.scrollLeft();
    if (scrollTop > targetOffset && sliderScrollLeft < sliderMaxScrollLeft) {
      $window.scrollTop(targetOffset);
      $slider.scrollLeft(sliderScrollLeft + horizontal_scrolling_speed);
    }
  }
});

$('section[data-type="background"]').each(function(){
        let $bgobj = $(this);
        $window.scroll(function() {
            let yPos = -($window.scrollTop() / $bgobj.data('speed'));
            let coords = 'center '+ yPos + 'px';
            $bgobj.css({ backgroundPosition: coords });
        });
    });
});
